/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include <QFileInfo>
#include <QSharedMemory>
#include <QtWidgets/QApplication>

#include <QtPlugin>

#include "LogWindow.h"

#if (defined QT_STATICPLUGIN)
Q_IMPORT_PLUGIN(qico)
#endif

#define SHARED_MEMORY_UUID "928fe311-dbb1-43e3-b659-8fa44ac50fcf"

int main(int argc, char *argv[])
{
	// Only allow one instance by creating a named shared memory segment.
	// If the create() call fails because the segment exists, then exit.
	QSharedMemory sharedMem(QStringLiteral(SHARED_MEMORY_UUID));
	if (!sharedMem.create(1) && (sharedMem.error() == QSharedMemory::AlreadyExists))
		exit(2);

#if (!defined QT_DEBUG)
	// Get path of current executable
	const QString executablePath = QFileInfo(LogWindow::getCurrentExecutablePath()).path();

	// Set plugin path to only be the path of the running executable
	// This means that an installed version won't pick up plugins from the QT SDK
	QApplication::setLibraryPaths(QStringList(executablePath));
#endif

	QApplication a(argc, argv);
	a.setQuitOnLastWindowClosed(false);
	LogWindow w;

	// Don't call w.show() - start hidden.

	return a.exec();
}
