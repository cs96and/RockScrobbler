# RockScrobbler

## Upgrade Note

**ROCKSCROBBLER NOW REQUIRES THE VISUAL C++ 2013 RUNTIME TO BE INSTALLED**

If you get an error about missing MSVCP120.dll or MSVCR120.dll, then please visit
<http://www.microsoft.com/en-gb/download/details.aspx?id=40784> to download and install the
redistributable.  The file you need is vcredist_x86.exe.

## Introduction

RockScrobbler is a small utility for Windows that sits in your system tray monitoring for
any inserted drives.  When a drive is inserted it looks for a .scrobbler.log and if it finds one,
scrobbles the information to [last.fm](http://last.fm)

The utility is designed to work with players running the [RockBox](http://www.rockbox.org) firmware,
but will work with any device that creates a .scrobbler.log file in the the root of the drive.

## Licence

RockScrobbler is licensed under the GPLv3 license. Please
see the file LICENSE.txt for full information on the licence.

This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (http://www.openssl.org/)

## Changelog

#### v0.15
* Fixed display of artist and track names in the log when they contain extended UTF-8 characters.
* Added hidden setting "tracksPerScrobble" to workaround current Last.fm problems.
* Updated to QT v5.5.0

#### v0.14
* Added update notification
* Updated to QT v5.3.0

#### v0.13
* Use HTTPS when logging in to last.fm
* Updated to QT v5.2.0
* Compile with Visual Studio 2013

#### v0.12
* Fixed bug where .scrobbler.log was not deleted if multiple volumes were mounted at the same time.

#### v0.11
* Added option to wait before scrobbling when a device is inserted
* Wait for 200ms between sending scrobble batches
* Wait for 5 seconds before re-trying a failed scrobble
* Updated to QT v5.1.0

#### v0.10
* Updated QT to v5.0.2

#### v0.9
* Only allow a single instance to run
* Added option to dump debugging output to log window
* Output ignored scrobble reason to log window
* Wait for all scrobble responses before displaying result to user
* Backup scrobbler.log files rather than deleting them
* After manual scrobble, ask user if they want to backup, delete or keep the scrobbler.log file
* Retry scrobble requests if they fail with certain error codes
* Updated QT to v4.8.4

#### v0.8.1
* Fixed version in about box
* Replaced installer with InnoSetup

#### v0.8
* Fixed timestamps of scrobbled tracks

#### v0.7
* Fixed issue scrobbling tracks containing a '+' character.
* Added option to use the users's avatar as the system tray icon.
* Added menu item to go to the user's last.fm profile page.
* Added icons to menu items.
* Updated to QT v4.8.1

#### v0.6
* Display avatar on settings screen when logged in.
* Disabled "Manual Scrobble" menu item when not logged in.
* Pop up an error if a scrobbleable drive is inserted and we are not logged in.
* Prevent multiple popup balloons appearing when scrobbling > 50 tracks.
* Set Login button to be the default button when it is displayed.

#### v0.5
* Fix issue when submitting more than 50 tracks.

#### v0.4
* Set POST data Content Type to "application/x-www-form-urlencoded" (fixes an issue with QT 4.8)

#### v0.3
* Updated to QT v4.8.0.

#### v0.2
* Include qico4.dll in installer.

#### v0.1
* Initial release.
