/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include "BitBucketApi.h"
#include "LogWindow.h"
#include "ui_LogWindow.h"
#include "DriveWatcher.h"
#include "Scrobbler.h"
#include "SettingsDialog.h"
#include "Version.h"
#include "ui_About.h"

#include <QDateTime>
#include <QDesktopServices>
#include <QFile>
#include <QIcon>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QTimer>
#include <QtWidgets/QAction>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMenu>

LogWindow::LogWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::LogWindow),
	m_pSystemTrayIcon(new QSystemTrayIcon(QIcon(QStringLiteral(":/icons/lastfm.ico")), this)),
	m_pDriveWatcher(new DriveWatcher(this)),
	m_pScrobbler(new Scrobbler(*this)),
	m_pBitBucketApi(new BitBucketApi(this)),
	m_pSettingsDialog(new SettingsDialog(*m_pScrobbler)),
	m_isManualScrobble(false),
	m_autoUpdateTimerId(0)
{
	ui->setupUi(this);

	// Set up system tray menu
#if (defined QT_DEBUG)
	m_pSystemTrayIcon->setToolTip(tr("Rock Scrobbler DEBUG"));
#else
	m_pSystemTrayIcon->setToolTip(tr("Rock Scrobbler"));
#endif
	connect(m_pSystemTrayIcon, &QSystemTrayIcon::activated, [this](QSystemTrayIcon::ActivationReason reason) { 
		if (reason == QSystemTrayIcon::DoubleClick)
			show();
	});

	QMenu * const pContextMenu = new QMenu(this);

	QAction * const pShowLogAction = pContextMenu->addAction(QIcon(QStringLiteral(":/icons/comments.png")), tr("Show log"), this, SLOT(show()));
	m_pManualScrobbleAction = pContextMenu->addAction(QIcon(QStringLiteral(":/icons/pencil.png")), tr("Manual scrobble"), this, SLOT(onManualScrobble()));
	pContextMenu->addAction(QIcon(QStringLiteral(":/icons/settings.png")), tr("Settings"), m_pSettingsDialog.data(), SLOT(show()));
	pContextMenu->addAction(tr("Check for update"), m_pBitBucketApi, SLOT(checkForUpdate()));
	pContextMenu->addSeparator();
	m_pGotoLastFmProfileAction = pContextMenu->addAction(tr("Last.fm profile"), this, SLOT(onGotoLastFmPage()));
	pContextMenu->addSeparator();
	pContextMenu->addAction(QIcon(QStringLiteral(":/icons/qt.ico")), tr("About Qt"), qApp, SLOT(aboutQt()));
	pContextMenu->addAction(QIcon(QStringLiteral(":/icons/last_fm.png")), tr("About"), this, SLOT(onShowAbout()));
	pContextMenu->addSeparator();
	pContextMenu->addAction(QIcon(QStringLiteral(":/icons/remove_2.png")), tr("Exit"), qApp, SLOT(quit()));

	pContextMenu->setDefaultAction(pShowLogAction);
	m_pManualScrobbleAction->setEnabled(m_pScrobbler->isLoggedIn());
	m_pGotoLastFmProfileAction->setEnabled(m_pScrobbler->isLoggedIn());

	m_pSystemTrayIcon->setContextMenu(pContextMenu);
	m_pSystemTrayIcon->show();

	// Connect drive watcher signal
	connect(m_pDriveWatcher, &DriveWatcher::inserted, this, &LogWindow::onDriveInserted);

	// Connect scrobbler signals
	connect(m_pScrobbler, &Scrobbler::authenticateSuccess, this, &LogWindow::onAuthenticateSuccess);
	connect(m_pScrobbler, &Scrobbler::avatar, this, &LogWindow::onAvatar);
	connect(m_pScrobbler, &Scrobbler::loggedOut, this, &LogWindow::onLogout);
	connect(m_pScrobbler, &Scrobbler::scrobbleComplete, this, &LogWindow::onScrobbleComplete);
	connect(m_pScrobbler, &Scrobbler::scrobbleError, this, &LogWindow::onScrobblerError);

	// Connect settings signals
	connect(m_pSettingsDialog.data(), &SettingsDialog::useAvatarAsIconChanged, this, &LogWindow::onUseAvatarAsIconChanged);
	connect(m_pSettingsDialog.data(), &SettingsDialog::shouldAutoCheckForUpdatesChanged, this, &LogWindow::onShouldAutoCheckForUpdatesChanged);

	// Connect BitBucketApi signals
	connect(m_pBitBucketApi, &BitBucketApi::error, this, &LogWindow::onScrobblerError);
	connect(m_pBitBucketApi, &BitBucketApi::newVersionAvailable, this, &LogWindow::onNewVersionAvailable);

	if (m_pSettingsDialog->shouldAutoCheckForUpdates())
	{
		onShouldAutoCheckForUpdatesChanged(true);

		// Check for update on startup, in 5 seconds time.
		QTimer::singleShot(5000, Qt::VeryCoarseTimer, m_pBitBucketApi, SLOT(checkForUpdate()));
	}
}

LogWindow::~LogWindow()
{
}

bool LogWindow::isDebugLoggingEnabled() const
{
	return m_pSettingsDialog->isDebugLoggingEnabled();
}

QString LogWindow::getCurrentExecutablePath()
{
	// Get path of currently running executable
	TCHAR filename[MAX_PATH+1];
	const DWORD filenameLength = GetModuleFileName(NULL, filename, MAX_PATH);

#if (defined UNICODE)
	return QString::fromUtf16(reinterpret_cast<ushort *>(filename), filenameLength);
#else
	return QString(filename, filenameLength);
#endif
}

void LogWindow::appendToLog(const QString &str)
{
	ui->textEdit->append(tr("%1 %2").arg(QDateTime::currentDateTime().toString(Qt::SystemLocaleShortDate)).arg(str));
}

void LogWindow::onDriveInserted(const QString &path)
{
	appendToLog(tr("Drive %1 inserted").arg(path));

	m_driveInsertionQueue.enqueue(path);

	if (m_pSettingsDialog->getWaitBeforeScrobble() == 0)
		onScrobbleTimer();
	else
		QTimer::singleShot(m_pSettingsDialog->getWaitBeforeScrobble() * 1000, this, SLOT(onScrobbleTimer()));
}

void LogWindow::onScrobbleTimer()
{
	if (!m_driveInsertionQueue.isEmpty())
	{
		const QString currentScrobbleFile = m_driveInsertionQueue.dequeue() % QStringLiteral("\\.scrobbler.log");

		if (QFile::exists(currentScrobbleFile))
		{
			if (m_pScrobbler->isLoggedIn())
			{
				m_currentScrobbleFile = currentScrobbleFile;
				m_isManualScrobble = false;

				m_pScrobbler->scrobbleFile(m_currentScrobbleFile, m_pSettingsDialog->getTracksPerScrobble());
			}
			else
			{
				onScrobblerError(tr("Scrobbler log found at %1, but not logged in").arg(currentScrobbleFile));
			}
		}
		else
		{
			appendToLog(tr("No scrobbler log found at %1").arg(currentScrobbleFile));
		}
	}
}

void LogWindow::onManualScrobble()
{
	const QStringList desktopLocations = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
	const QString filename = QFileDialog::getOpenFileName(this, tr("Open scrobble log"), (desktopLocations.isEmpty() ? QString() : desktopLocations.front()));

	if (!filename.isNull())
	{
		m_currentScrobbleFile = filename;
		m_isManualScrobble = true;

		m_pScrobbler->scrobbleFile(filename, m_pSettingsDialog->getTracksPerScrobble());
	}
}

void LogWindow::onGotoLastFmPage()
{
	QDesktopServices::openUrl(m_pScrobbler->getUserUrl());
}

void LogWindow::onShowAbout()
{
	Ui::About aboutUi;
	QScopedPointer<QDialog> pAboutDialog(new QDialog);

	aboutUi.setupUi(pAboutDialog.data());
	// Set version string
	aboutUi.textLabel->setText(aboutUi.textLabel->text().arg(ROCK_SCROBBLER_VERSION));

	// pAboutDialog will be deleted when OK or the close button is clicked.
	connect(pAboutDialog.data(), &QDialog::accepted, pAboutDialog.data(), &QDialog::deleteLater);
	connect(pAboutDialog.data(), &QDialog::rejected, pAboutDialog.data(), &QDialog::deleteLater);

	// Take ownership of pointer and show the about box.
	pAboutDialog.take()->show();
}

void LogWindow::onAuthenticateSuccess(const QString &/*username*/)
{
	m_pManualScrobbleAction->setEnabled(true);
	m_pGotoLastFmProfileAction->setEnabled(true);
}

void LogWindow::onAvatar(const QPixmap &avatar)
{
	if (!avatar.isNull())
		m_pGotoLastFmProfileAction->setIcon(avatar);
	else
		m_pGotoLastFmProfileAction->setIcon(QIcon());

	if (!avatar.isNull() && m_pSettingsDialog->shouldUseAvatarAsIcon())
		m_pSystemTrayIcon->setIcon(avatar);
	else
		m_pSystemTrayIcon->setIcon(QIcon(QStringLiteral(":/icons/lastfm.ico")));
}

void LogWindow::onLogout()
{
	m_pSystemTrayIcon->setIcon(QIcon(QStringLiteral(":/icons/lastfm.ico")));
	m_pManualScrobbleAction->setEnabled(false);
	m_pGotoLastFmProfileAction->setEnabled(false);
	m_pGotoLastFmProfileAction->setIcon(QIcon());
}

void LogWindow::onScrobbleComplete(quint32 nScrobbled, quint32 nIgnored, quint32 nErrors)
{
	QSystemTrayIcon::MessageIcon icon;

	QString logString = tr("Scrobbled %1 tracks").arg(nScrobbled);
	if (nIgnored > 0)
		logString += tr(" / %1 ignored").arg(nIgnored);

	if (nErrors == 0)
	{
		icon = QSystemTrayIcon::Information;
	}
	else
	{
		logString += tr(" / %1 errors").arg(nErrors);
		icon = (((nScrobbled > 0) || (nIgnored > 0)) ? QSystemTrayIcon::Warning : QSystemTrayIcon::Critical);
	}

	appendToLog(logString);

	if (nErrors > 0)
		logString += tr("\nView log for more information about errors");

	if (m_messageClickConnection)
		disconnect(m_messageClickConnection);

	m_messageClickConnection = connect(m_pSystemTrayIcon, &QSystemTrayIcon::messageClicked, this, &LogWindow::show);
	
	m_pSystemTrayIcon->showMessage(tr("Rock Scrobbler"), logString, icon, 30000);

	if (!m_currentScrobbleFile.isEmpty())
	{
		if (m_isManualScrobble)
		{
			QMessageBox msgBox(QMessageBox::Question, tr("Rock Scrobbler"),
				tr("Do you want to backup, delete or keep the manually scrobbled file\n%1").arg(m_currentScrobbleFile),
				QMessageBox::NoButton, this);

			QPushButton * const pBackupButton = msgBox.addButton(tr("Backup"), QMessageBox::YesRole);
			QPushButton * const pDeleteButton = msgBox.addButton(tr("Delete"), QMessageBox::YesRole);
			msgBox.addButton(tr("Keep"), QMessageBox::NoRole);
			msgBox.setDefaultButton(pBackupButton);
			msgBox.exec();

			if (msgBox.clickedButton() == pBackupButton)
			{
				backupFile(m_currentScrobbleFile);
			}
			else if (msgBox.clickedButton() == pDeleteButton)
			{
				QFile::remove(m_currentScrobbleFile);
			}
		}
		else
		{
			backupFile(m_currentScrobbleFile);
		}
	}

	m_currentScrobbleFile.clear();
}

void LogWindow::onScrobblerError(const QString &error)
{
	if (m_messageClickConnection)
		disconnect(m_messageClickConnection);

	m_messageClickConnection = connect(m_pSystemTrayIcon, &QSystemTrayIcon::messageClicked, this, &LogWindow::show);

	appendToLog(error);
	m_pSystemTrayIcon->showMessage(tr("Rock Scrobbler error"), error, QSystemTrayIcon::Warning, 30000);
}

void LogWindow::onUseAvatarAsIconChanged(bool useAvatarAsIcon)
{
	if (useAvatarAsIcon && m_pScrobbler->isLoggedIn())
		m_pSystemTrayIcon->setIcon(m_pScrobbler->getCurrentAvatar());
	else
		m_pSystemTrayIcon->setIcon(QIcon(QStringLiteral(":/icons/lastfm.ico")));
}

void LogWindow::onShouldAutoCheckForUpdatesChanged(bool shouldAutoCheckForUpdates)
{
	// Kill any existing timer
	if (0 != m_autoUpdateTimerId)
	{
		killTimer(m_autoUpdateTimerId);
		m_autoUpdateTimerId = 0;
	}

	if (shouldAutoCheckForUpdates)
	{
		// Check once per day.
		m_autoUpdateTimerId = startTimer(60 * 60 * 24 * 1000, Qt::VeryCoarseTimer);
	}
}

void LogWindow::onNewVersionAvailable(const QString &newVersion)
{
	if (m_messageClickConnection)
		disconnect(m_messageClickConnection);

	m_messageClickConnection = connect(m_pSystemTrayIcon, &QSystemTrayIcon::messageClicked, []{
		QDesktopServices::openUrl(QUrl(QStringLiteral("https://bitbucket.org/cs96and/rockscrobbler")));
	});

	m_pSystemTrayIcon->showMessage(tr("Rock Scrobbler Version %1 Available").arg(newVersion),
		tr("Click here to download it"),
		QSystemTrayIcon::Information, 30000);
}

void LogWindow::backupFile(const QString path)
{
	const quint32 MIN_BACKUP_NUMBER = 1;
	const quint32 MAX_BACKUP_NUMBER = 3;

	// Roll existing backups
	quint32 i = MAX_BACKUP_NUMBER;
	while (i > MIN_BACKUP_NUMBER)
	{
		const QString toFileName = QStringLiteral("%1.%2").arg(path).arg(i);
		const QString fromFileName = QStringLiteral("%1.%2").arg(path).arg(--i);

		if (QFile::exists(toFileName))
			QFile::remove(toFileName);

		QFile::rename(fromFileName, toFileName);
	}

	// Backup the file
	QFile::rename(path, QStringLiteral("%1.%2").arg(path).arg(i));
}

void LogWindow::timerEvent(QTimerEvent * pEvent)
{
	if (pEvent->timerId() == m_autoUpdateTimerId)
		m_pBitBucketApi->checkForUpdate();
	else
		QMainWindow::timerEvent(pEvent);
}
