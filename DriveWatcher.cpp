/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include "DriveWatcher.h"

#include <QString>

#include <comutil.h>
#include <comdef.h>

DriveWatcher::DriveWatcher(QObject *parent) :
	QObject(parent)
{
	// No need to call CoInitializeEx or CoInitializeSecurity, it seems QT has already done it for us

	// connect to WMI
	HRESULT result = CoCreateInstance(CLSID_WbemAdministrativeLocator, NULL, CLSCTX_INPROC_SERVER, IID_IWbemLocator, reinterpret_cast<void**>(&m_pLocator));
	if (FAILED(result))
		throw std::runtime_error("Instantiation of IWbemLocator failed");

	// connect to local service with current credentials
	result = m_pLocator->ConnectServer(L"root\\cimv2", NULL, NULL, NULL, WBEM_FLAG_CONNECT_USE_MAX_WAIT, NULL, NULL, &m_pService);
	if (FAILED(result))
		throw std::runtime_error("Couldn't connect to service");

	// execute query
	m_pNotificationSink = new CNotificationSink(*this);
	result = m_pService->ExecNotificationQueryAsync(L"WQL", L"SELECT * FROM Win32_VolumeChangeEvent WHERE EventType = 2", 0, NULL, m_pNotificationSink);
	if (FAILED(result))
		throw std::runtime_error("WQL Query failed");
}

//
// CNotificationSink
//

DriveWatcher::CNotificationSink::CNotificationSink(DriveWatcher &driveWatcher) :
	m_count(0),
	m_driveWatcher(driveWatcher)
{
}

DriveWatcher::CNotificationSink::~CNotificationSink()
{
}

HRESULT STDMETHODCALLTYPE
DriveWatcher::CNotificationSink::QueryInterface(REFIID riid, void __RPC_FAR * __RPC_FAR *ppvObject)
{
	if ((IID_IUnknown == riid) || (IID_IWbemObjectSink == riid))
	{
		*ppvObject = this;
		AddRef();
		return S_OK;
	}
	else
	{
		*ppvObject = NULL;
		return E_NOINTERFACE;
	}
}

ULONG STDMETHODCALLTYPE
DriveWatcher::CNotificationSink::AddRef()
{
	return InterlockedIncrement(&m_count);
}

ULONG STDMETHODCALLTYPE
DriveWatcher::CNotificationSink::Release()
{
	if (m_count == 0)
	{
		delete this;
		return 0;
	}
	else
	{
		return InterlockedDecrement(&m_count);
	}
}

HRESULT STDMETHODCALLTYPE
DriveWatcher::CNotificationSink::Indicate(long lObjectCount, IWbemClassObject** apObjArray)
{
	for (long i = 0; i < lObjectCount; ++i)
	{
		_variant_t variant;
		HRESULT result = apObjArray[i]->Get( L"DriveName", 0, &variant, NULL, NULL );
		if (SUCCEEDED(result))
		{
			const _bstr_t drive = variant;
			const ushort * pDriveUShort = reinterpret_cast<const ushort *>(static_cast<const wchar_t *>(drive));
			m_driveWatcher.emit inserted(QString::fromUtf16(pDriveUShort, drive.length()));
		}
	}

	return WBEM_S_NO_ERROR;
}

HRESULT STDMETHODCALLTYPE
DriveWatcher::CNotificationSink::SetStatus(long lFlags, HRESULT hResult, BSTR strParam, IWbemClassObject* pObjectParam)
{
	UNREFERENCED_PARAMETER(lFlags);
	UNREFERENCED_PARAMETER(hResult);
	UNREFERENCED_PARAMETER(strParam);
	UNREFERENCED_PARAMETER(pObjectParam);

	return WBEM_S_NO_ERROR;
}
