/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

	Rock Scrobbler is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Rock Scrobbler is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#include "BitBucketApi.h"
#include "Version.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStringBuilder>

#include <set>

#include <assert.h>

namespace
{
const QString CURRENT_VERSION(QStringLiteral(ROCK_SCROBBLER_VERSION));
const QString USER_NAME(QStringLiteral("cs96and"));
const QString REPO_NAME(QStringLiteral("rockscrobbler.git"));
const QString BASE_URL(QStringLiteral("https://bitbucket.org/api/1.0"));
const QUrl GET_TAGS_URL(BASE_URL % QStringLiteral("/repositories/") % USER_NAME % QChar('/') %
	REPO_NAME % QStringLiteral("/tags"));
}

BitBucketApi::BitBucketApi(QObject * parent) :
	QObject(parent),
	m_pNetworkAccessManager(new QNetworkAccessManager(this))
{
}

BitBucketApi::~BitBucketApi()
{
}

void BitBucketApi::checkForUpdate()
{
	QNetworkRequest request(GET_TAGS_URL);
	request.setRawHeader(QByteArrayLiteral("User-Agent"), QByteArrayLiteral("RockScrobbler " ROCK_SCROBBLER_VERSION));

	QNetworkReply * const pReply = m_pNetworkAccessManager->get(request);
	connect(pReply, &QNetworkReply::finished, this, &BitBucketApi::onGetTagsReply);
}

void BitBucketApi::onGetTagsReply()
{
	// Ensure reply gets deleted later
	sender()->deleteLater();

	QNetworkReply * const pNetworkReply = dynamic_cast<QNetworkReply *>(sender());
	assert(nullptr != pNetworkReply);

	QString errorString;

	if (pNetworkReply->error() == QNetworkReply::NoError)
	{
		const QByteArray data = pNetworkReply->readAll();
		QJsonParseError jsonParseError;

		QJsonDocument jsonDoc = QJsonDocument::fromJson(data, &jsonParseError);

		if (!jsonDoc.isNull())
		{
			if (jsonDoc.isObject())
			{
				typedef std::set<QString, VersionLess> VersionSet;
				VersionSet versionSet;

				const QJsonObject jsonObject = jsonDoc.object();

				for (QJsonObject::const_iterator iter = jsonObject.constBegin(), iterEnd = jsonObject.constEnd(); iterEnd != iter; ++iter)
					versionSet.insert(iter.key());

				if (!versionSet.empty() && VersionLess()(CURRENT_VERSION, *versionSet.rbegin()))
					emit newVersionAvailable(*versionSet.rbegin());
			}
			else
			{
				errorString = tr("received data is not a JSON object");
			}
		}
		else
		{
			errorString = jsonParseError.errorString();
		}
	}
	else
	{
		errorString = pNetworkReply->errorString();
	}

	if (!errorString.isNull())
		emit error(tr("Failed to check for updates:\n%1").arg(errorString));
}

inline bool BitBucketApi::VersionLess::operator()(const QString &lhs, const QString &rhs) const
{
	const QStringList lhsParts = lhs.split(QChar('.'));
	const QStringList rhsParts = rhs.split(QChar('.'));

	const int minSize = qMin(lhsParts.size(), rhsParts.size());

	// Loop through each part of the version number
	for (int i = 0; i < minSize; ++i)
	{
		// Attempt to convert both sides to uints
		bool lhsOk, rhsOk;
		const uint lhsVersion = lhsParts[i].toUInt(&lhsOk);
		const uint rhsVersion = rhsParts[i].toUInt(&rhsOk);

		if (lhsOk && rhsOk)
		{
			// If both sides contain numbers, then compare numerically
			if (lhsVersion < rhsVersion)
				return true;
			else if (lhsVersion > rhsVersion)
				return false;
		}
		else
		{
			// Compare alphabetically (case insensitive)
			const int result = lhsParts[i].compare(rhsParts[i], Qt::CaseInsensitive);
			if (result < 0)
				return true;
			else if (result > 0)
				return false;
		}
	}

	return (lhsParts.size() < rhsParts.size());
}

