/*
	Copyright 2011-2014 Alan Davies.

	This file is part of Rock Scrobbler.

    Rock Scrobbler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rock Scrobbler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rock Scrobbler.  If not, see <http://www.gnu.org/licenses/>.

	In addition, as a special exception, the copyright holders give
	permission to link the code of portions of this program with the
	OpenSSL library under certain conditions as described in each
	individual source file, and distribute linked combinations
	including the two.

	You must obey the GNU General Public License in all respects
	for all of the code used other than OpenSSL.  If you modify
	file(s) with this exception, you may extend this exception to your
	version of the file(s), but you are not obligated to do so.  If you
	do not wish to do so, delete this exception statement from your
	version.  If you delete this exception statement from all source
	files in the program, then also delete it here.
*/

#ifndef DRIVEWATCHER_H
#define DRIVEWATCHER_H

#include <QObject>

#include <Wbemidl.h>
#include <atlcomcli.h>

class QString;

class DriveWatcher : public QObject
{
	Q_OBJECT

public:
	explicit DriveWatcher(QObject *parent = 0);

signals:
	void inserted(const QString &path) const;

private:
	class CNotificationSink : public IWbemObjectSink
	{
	public:
		CNotificationSink(DriveWatcher &driveWatcher);
		virtual ~CNotificationSink();

		// from IUnknown
		virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void __RPC_FAR * __RPC_FAR *ppvObject) Q_DECL_OVERRIDE;
		virtual ULONG STDMETHODCALLTYPE AddRef() Q_DECL_OVERRIDE;
		virtual ULONG STDMETHODCALLTYPE Release() Q_DECL_OVERRIDE;
		// from IWbemObjectSink
		virtual HRESULT STDMETHODCALLTYPE Indicate(long lObjectCount, IWbemClassObject** apObjArray) Q_DECL_OVERRIDE;
		virtual HRESULT STDMETHODCALLTYPE SetStatus(long lFlags, HRESULT hResult, BSTR strParam, IWbemClassObject* pObjectParam) Q_DECL_OVERRIDE;

	private:
		long			m_count;
		DriveWatcher	&m_driveWatcher;
	};

	CComPtr<IWbemLocator>		m_pLocator;
	CComPtr<IWbemServices>		m_pService;
	CComPtr<CNotificationSink>	m_pNotificationSink;
};

#endif // DRIVEWATCHER_H
